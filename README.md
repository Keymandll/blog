# Posts

| Date       | Title                             | Brief | Read |
| ---------- | --------------------------------- | ----- | ---- |
| 03/04/2023 | Application Secrets Handling in a Kubernetes Environment | How to best handle application secrets in a Kubernetes (or in general Cloud) environment. | [Read](./posts/02042023-Application_Secrets_Handling_Kubernetes/index.md) |
| 23/02/2023 | The Impact of Security on Software Security (Part 1) | The impact of “security by obscurity” on software security. | [Read](./posts/23022023-The_Impact_of_Security_on_Software_Security_Part_1/index.md) |
| 25/11/2022 | Koa.JS Invulnerability Analysis Update  | Adding more context to the previous Koa.JS invulnerability analysis and correcting some mistakes regarding Sonatype and Dependency Track. | [Read](./posts/25112022-KoaJS_Invulnerability_Analysis_Update/index.md) |
| 28/10/2022 | Koa.JS Invulnerability Analysis   | Analysing another lazy "vulnerability" report, this time about Koa.JS. | [Read](./posts/28102022-KoaJS_Invulnerability_Analysis/index.md) |
| 27/10/2022 | Software Composition Analysis for Startups | Putting together a very simple but effective dependency auditing platform using open-source tools. | [Read](./posts/27102022-Software_Composition_Analysis_for_Startups/index.md) |
| 11/06/2022 | The Modern Day Witch Hunt         | Lamenting the angry mob mentality and ignorance of the cybersecurity scene. | [Read](./posts/11062022-The_Modern_Day_Witch_Hunt/index.md) |
| 03/06/2022 | CVE-2022–29622: (In)vulnerability Analysis | A detailed analysis proving that CVE-2022–29622 falsely accused Formidable of being vulnerable. | [Read](./posts/03062022-Invulnerability_Analysis-CVE-2022%E2%80%9329622/index.md) |
| 31/05/2022 | CyberSecurity: The Next Supply Chain Vulnerability? | In this post I'm highlighting serious problems with the cybersecurity industry. | [Read](./posts/31052022-The_Next_Supply_Chain_Vulnerability/index.md) |
| 01/05/2019 | Vulnerability Isolation Principle | The principle focuses on reducing the potential impact of vulnerabilities and improving the organization’s response (remediation) time. | [Read](./posts/01052019-Vulnerability_Isolation_Principle/index.md) |
