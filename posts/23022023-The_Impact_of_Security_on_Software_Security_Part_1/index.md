# The Impact of Security on Software Security (Part 1)

Author: [Zsolt Imre](http://zsoltimre.com)

Date: 23/02/2023

# Tl;dr

The easier to test an application and the cleaner, more transparent and consistent the behaviour of the application is, the more likely we can build secure and reliable applications. Simply, because it is easier to spot issues. When it comes to security features and fixes, we must be mindful of their implementation to avoid shooting ourselves in the foot.

The first chapter (this post) will focus on the impact of “security by obscurity” on software security.

# Security by Obscurity

There is a decades-old security practice that some refer to as “security by obscurity”. The idea is simple: there are always going to be vulnerabilities, but measures can be taken to make it more difficult for an attacker to identify and exploit these vulnerabilities. These measures are often in place to confuse an attacker or the tools used by the attacker by hiding or altering certain information and behavioural patterns.

A nice collection of various techniques can often mess with the attacker and the automated tools used. As a result, the attacker may miss vulnerabilities. However, there are plenty of attackers that expect these kinds of measures to be in place so they configure or modify their tools to ignore certain behaviours and keep going. Of course, this will result in more noise, thus detecting ongoing attacks in theory becomes easier due to the increased visibility. I’m saying “in theory” because in practice this depends on a lot of things. For example:

 * Do we have detection tools in place that are configured properly to pick up the attacks?
 * Do we have professionals who can make sense of the alerts produced by these tools and know what to do?
 * Do we have qualified people to perform manual analysis when required?

My experience says that most often (but not always!) an attacker does not have to be as careful as we would like to believe. Even at mature organisations, the SOC teams are struggling with detecting and stopping real, ongoing attacks, despite the many tools in place to help with the task.

I will discuss some of the problems with security by obscurity using two examples:

 1. An IIS web server serving an ASP application returning an Nginx identification string in the `Server` response header and an `X-Powered-By` header with the value `PHP/7.3.1`. The `.asp` extension is hidden in all the requests. (However, due to how the configuration was implemented pages/resources are also served if the `.asp` extension was present.)
 2. A web service or web application that responds with a status code that hides underlying issues. An HTTP 500 (Internal Server Error) response code may signal the attacker that he/she just touched on something while tampering with the requests sent to the application. Returning an HTTP 200 (Ok) response even in case of an error may suggest to an attacker that there is nothing to see there.

Imagine that one day a member of the security team is looking at a weird HTTP request and sees that a request was submitted to `execute.asp` with a from parameter value of `https://github.com/tennc/webshell/blob/master/asp/webshell.asp`. Going back to example #1, let’s say the web server was indeed a Microsoft IIS web server running an ASP application and execute.asp was there and vulnerable. The web server however returns the `Server` header stating that it’s an `Nginx` web server and the `X-Powered-By: PHP/7.3.1` header indicates that the server serves one or more PHP applications. The security team member may decide to look at the reports produced by a vulnerability scanner to see if the vulnerability was there and what the service fingerprinting says about the service. This person may conclude that a successful attack would require an ASP interpreter, but what they’ve got is a PHP interpreter, thus the request is not a threat. If you think this scenario is unlikely, I can tell that I’ve seen something very similar happen.

Along the lines of the same example, imagine someone hiring a security consulting firm to perform a black-box penetration test. As usual, the budget is not infinite so there’s a set timeframe (usually a lot shorter than required) to perform the assessment. The consultants tasked with the engagement will perform service fingerprinting as part of the reconnaissance step and make decisions on the next steps based on the results. They will see that there’s no point looking for ASP application vulnerabilities and they will continue on the “PHP route”, which obviously will lead to nowhere.

Finally, let’s talk a bit about example #2 and its potential impact. There are two quality assurance practices I would like to mention: integration testing and system testing. These are both there to check whether a component, feature, application or system operates according to the expectations. If a web application or service keeps returning a `200 OK` HTTP response code every time it encounters a fatal error that would require a `500 Internal Error` response, problems that negatively impact availability and reliability (and potentially security) may go unnoticed during testing.

*On a side note: Like it or not, from a business perspective availability and reliability is a lot more important than confidentiality. I suspect some security professionals will jump on this statement, so let me share my reasoning: availability issues have a direct and immediate financial impact. If a business releases a product or feature that does not work or is full of bugs, chances are they cannot acquire customers or they may lose existing customers. If there’s a confidentiality-related issue, it may negatively impact reputation and may result in some financial damage, but as we can see from the past, most of the companies that had a breach of confidentiality are still in business. Therefore, it’s quite risky to advocate for security practices that may negatively impact availability and reliability.*

In the context of the previously mentioned examples, you could have a web proxy (e.g. Nginx) change the HTTP headers and response codes based on specific rules, instead of messing with the codebase or the web server/service configuration. When it comes to any kind of testing (be it in-house or 3rd party), those would always be done by testing the applications and services in scope directly, bypassing the said proxy.

However, if I was a decision-maker, I would rather have the security team help out engineers with finding and fixing bugs and vulnerabilities rather than putting effort into simply hiding them from everyone.

# Conclusion

If you insist on security by obscurity, you must be mindful of the implementation so it does not negatively impact the business and security.

As a starter, I would set the following general rules:

 * Avoid security by obscurity as much as you can. If you cannot:
 * Security by obscurity MUST NOT be the first line of defence.
 * Security by obscurity MUST NOT be part of the product/application codebase.
 * The application/service/system MUST be tested WITHOUT security by obscurity mechanisms in place.

# Upcoming Topics

Some of the related topics you can expect in the future (as my time allows) are: “Missing the Root Cause”, “Over-engineering”, “Adding Yet Another Layer” and “The Problem with Black-box Security Assessments”.

# Notes

I usually review all my posts several times before and after publishing to make sure it says what I wanted to say. I often make minor corrections and updates post-publishing.

Also, excuse me for the title, I could not come up with anything better.
