# The Modern Day Witch Hunt (CyberSecurity)

Author: [Zsolt Imre](http://zsoltimre.com)

Date: 11/06/2022

Ever since I was a kid I was fascinated by witches. To be completely honest, at first, I was terrified, only later got fascinated. As I grew older only I realized how disgusting those witch hunts may have been and how very likely it was that I would have ended up being burnt by the scared, angry mob, along with my chemistry and physics teacher. Even though, none of us were witches.

I suspect most of those who got hanged or burnt were nothing more than people that knew something the mob could not comprehend and, as we know, the thing human beings are the most terrified of is the unknown. It must have been a situation like if in the primary school a bunch of 10 years old kids decided to hang up the chemistry teacher on a tree because the teacher [conjured a monster](https://youtu.be/8vyboVwyzfU?t=479) they felt threatened by. By the way, I recommend watching the entire video, it’s all witchcraft!

But why the bloody hell are you talking about witches?! — you may ask. Just to help with setting up the context to help you understand my perspective later. Actually, I do not even want to talk about witches, I was to talk about the angry, blood-thirsty mob attacking and burning anything they do not understand.

You may have heard about CVE-2022–29622 and my analysis of it which you can find [HERE](../03062022-Invulnerability_Analysis-CVE-2022%E2%80%9329622/index.md). I do not really want to talk about CVE-2022–29622, but I do believe it is a great example, and that some of the related discussions are worth analyzing further as they are great examples that clearly indicate a problem or a series of problems.

Having said the above, my “fight” was not about Formidable, not even about this specific CVE. It was about something greater that only a very few within the security industry could understand. That is what this post is going to be about.

# The Angry Mob

In my mind, the existence of this CVE and some of the surrounding discussions, some are [public](https://www.reddit.com/r/netsec/comments/v6rccw/cve202229622_invulnerability_analysis/), some are somewhat more private (LinkedIn) looks and sound very similar to the scene of [Monty Python when the mob claims they found a witch](https://www.reddit.com/r/netsec/comments/v6rccw/cve202229622_invulnerability_analysis/).

In this scenario, our witch is the [Formidable](https://github.com/node-formidable/formidable) NodeJS library, and the mob are those claiming it is vulnerable as discussed by the CVE. Some of the discussions sound to me, just like what is shown in the video above. Something has the characteristics of a witch, therefore it must be a witch. It does not matter whether we dressed someone (in this case something) up as a witch or not, now it looks like one, so it must be one. If you were wondering, I believe that in this scenario I’m probably closest to the dude asking the question “How do you know she’s a witch?” — or at least, for now, that’s where I would like to position myself. However, I’m a security professional who is very passionate about vulnerability management and I do believe I’m a fair, competent judge, unlike any of those in the video above.

The scene Monty Python shows is called a witch trial during which a group of people made a seemingly scientific attempt to determine whether the accused was indeed a witch or was not. I would like to emphasize: “seemingly scientific”. Even a half-decent scientist knows as it’s been proven during the history of humankind that things we once considered facts turned out to be incorrect or inaccurate. Fortunately, there were people that did not stop questioning things. They kept looking, challenging the status quo, analyzing and learning, and by doing this, they have dragged us out of the muddy pit of the dark ages. But, we were always, constantly living in such dark ages, even now. We will just have to take a look back ten, twenty or a hundred years from now to realize that.

Other than just focusing on analyzing the claims regarding Formidable, doing source code review and dynamic analysis I was doing something else as well: I was trying really hard to understand why, despite the lack of any technically sound proof, some people still insisted the library was vulnerable.

Instead of ignoring these people and moving on while saying they probably lack the experience and the technical understanding, I decided I will try to figure out why they think the way they do. This is because even though I have decades of experience too and that comes with some ego, one thing that I’ve learnt is once I overcome my ego, suddenly I’m presented with a great opportunity, in this context: I’m either going to learn something new or maybe I will be able to teach someone something new. However, it is crucial that I stay objective, I should not take things at face value and, if needed, I have to take a look under the hood to find where the problem lies.

I have to tell you, I had a really difficult time trying to reverse engineer some people’s thought processes and find the root cause of those lines of thought. I probably would not have made any progress without getting the perspective of more than one person. Until one day, thanks to someone’s comment on GitHub I found my holy grail. I leave a screenshot below.

![Screenshot 1](./assets/screenshot-1.webp)

I have suspected from previous LinkedIn discussions that the flaw in certain people’s understanding will be a contextual problem. And the above snippet seems to prove that. By the way, I **do not** blame nor shame him for having posted this, because **he’s not wrong at all**, but, he’s not wrong **in a different context**. And this scenario he mentioned I believe is a great way of pointing out a problem in how some people may approach this topic.

# Context Ignorance is the Root of All Evil

Let’s analyze things objectively.

“We have two users, Alice and Bob”. — Based on my analysis so far, I have not seen any indication that Formidable has a concept of users. So, **Alice and Bob live in a totally different dimension**. A higher dimension, which in this scenario is called a web application.

![Screenshot 2](./assets/screenshot-2.webp)

The next bullet point, without quoting the whole sentence says: “the website doesn’t randomize the file name”. By now, we have two bullet points talking about a website.

Regarding the next bullet point: Indeed, it is an interaction between two users via a web application. However, Formidable does not facilitate file exchanges between users as:

 1. As mentioned earlier, it does not even have a concept of users
 2. By design, it does not have a file exchange/sharing feature implemented as that’s out of the scope when it comes to the responsibilities of the library

Why the above is interesting is that it makes it clear that contextually he is in web application land.

The last bullet point is an interesting one too. It suggests someone may be able to overwrite someone else’s files. If you check my responses to his comment starting from [HERE](https://github.com/node-formidable/formidable/issues/856#issuecomment-1150369027), you may notice that I performed a check and concluded that the last bullet point mentioned is not a valid scenario due to how the library worked by default and even with a non-default configuration.

It very much seems to me that not only he, but some others also confused Formidable (a software library) with a Content Management System (CMS).

At the same time, I really appreciate his comment and it was a very valuable contribution because as a result, we analyzed things further, looking at the library from a different perspective. Let’s talk about this for a second. As you will see in the discussion, there seemed to be a general consensus that the original CVE made no sense. However, when looking at how the file names were generated with the default configuration [something interesting started to emerge](https://github.com/node-formidable/formidable/issues/856#issuecomment-1150391009). Things took a totally different direction, one that actually made sense, unlike CVE-2022–29622.

People arguing with me tend to come up with scenarios about a fictional web application that may follow insecure practices and blame a file upload library for the mistakes of a poorly implemented web application that deliberately uses a non-default configuration of the library, does not implement proper access control and security wasn’t even an afterthought. They also completely ignore the fact that this library is not a web application. Like in Monty Python’s witch trial scene, they dressed up Formidable so that it looked like a witch (in this case a vulnerable web app), they put on the ridiculous nose and started chanting “Witch! Witch! Witch!”.

I do understand that people try to be pragmatic, and analyse things using established frameworks, following OWASP, calculating CVSS scores and so on. However, it is clear that OWASP is outdated, the world has changed a lot and you cannot necessarily force some of these outdated things on everything anymore. Want to see a good example?

First, you may want to read [THIS](https://github.com/node-formidable/formidable/issues/856#issuecomment-1151594291). Please note, by this time we were talking about something different than what the CVE was about. It’s a well-reasoned CVSS calculation that went exactly according to the books, with some added common sense that already made him stand out from the crowd. At the same time, the calculation ignored context. “Bloody hell, this again…”, right? Now, here is my reasoning:

![Screenshot 3](./assets/screenshot-3.webp)

You probably do not know, but I used to be a penetration tester and I had the pleasure to work with some companies that specifically asked to use their own risk calculation formula as CVSS was not suitable for such a thing. To be honest, it may easily be that CVSS is not bad at all if you can factor in some common sense. As can be seen in the screenshot above, with some common sense it is not so difficult to come up with a context-aware, reasonable CVSS score. But, you would have to go the extra mile.

We can analyze things further by looking at how we are using Formidable in our environment. Let’s jump right in.

# The Software Engineer versus Penetration Tester

If you were a Penetration Tester assessing our software in a black-box manner, given a user account, you would find that you can upload arbitrary files. Even though contextually we expect Wireshark capture files, you can still upload anything and it is going to appear on the files system, even if only briefly. What happens next is that we pass that file to `tshark`. The web application will explicitly tell you what it expects you to upload so you could suspect either we have something proprietary to deal with network captures, or we use something else to deal with it, or, the combination of the two. It is the one in the middle. Anyway, a pentester, even without doing anything more will immediately feel satisfaction after figuring out that he/she can upload just any file: “Whatever happens, at least I have something to put in the report already: arbitrary file upload!”

Let’s assume for a second that this is all that you could achieve during your assessment. You hand over your report to me (playing the role of an engineer/engineering lead/CTO — pick one). I take a look and see that the report says: “You have a critical vulnerability (CVSS 9.8) as your application allowed uploading arbitrary files.”. And this is what the follow-up discussion could look like, starting with me saying:

 - What was the impact that justified the 9.8 score and the critical severity?
 - Well if the application did “this” or “that” with the file, it would be that bad. Even OWASP lists a lot of scenarios in case this could be catastrophic.
 - I do understand that! IF my application was an ice cream machine, it would give me ice cream too, but it’s not. Did the fact that you uploaded a file have any impact?
 - Well, no. At least I do not have any proof at the moment. I would need more time to investigate.
 - Sure, I can give you some more time. How much time do you need?
 - Well, I do not know because there are so many things to try.


It’s a very uncomfortable discussion for any penetration tester and a very frustrating one for anyone on the receiving end of such a report.

First of all, you were trying to convince me there was a vulnerability. But, without any negative impact, in reality, it was just a file upload. I’m not interested in theoretical scenarios and how an arbitrary file upload may impact **someone else’s application**. And I’m saying all these things to you as a security professional myself with a complete understanding of where arbitrary file upload can lead in certain settings. I do understand it is an **attack vector** and now I understand that **you confused an attack vector with a vulnerability**. What was your added value? I can look up OWASP for myself. I did not pay 1000–1200 GBP per day for you just to provide me with a quote from OWASP.

Let’s say I gave you some more time to play with our software. You figured out that we are passing all uploaded network captures to `tshark`. You end up looking at the [Wireshark Security Advisories](https://www.wireshark.org/security/) and get all excited about all the opportunities. No surprise, I would be excited too! Finally, there is a chance you can actually do something that has an impact. Let’s say you could exploit [THIS](https://www.wireshark.org/security/wnpa-sec-2022-01.html). I chose this because successfully exploiting this would definitely have an impact (Denial of Service). What would you say in your report? Would you say the below?

> “An arbitrary file upload vulnerability in your software allowed an attacker to trigger a denial of service condition by exhausting all CPU resources.”

I would accept this as being of Critical severity with a score of 9.8. But, just because I accepted it, I do not think you are correct. I do understand that the problem is with `tshark`, and based on the fact you could do a DoS using a specially crafted network capture you must suspect this as well. Still, I could have some follow-up questions:

 * If we both know that the problem is with `tshark`, how does that make arbitrary file upload suddenly a vulnerability? Correct me if I’m wrong, but your previous attempt at uploading arbitrary files did not result in a denial of service.
 * Or, are you saying that Formidable processes network captures? I haven’t seen any such thing in its source code.

Then, just to be safe, I set up the system for you so that the uploaded files are not passed to `tshark`. Then, I asked you to upload the same, malicious network capture again. After having uploaded the file you would not see any signs of extensive CPU usage.

Do you see where I’m going with this? If you were a professional, you would realize the most appropriate way to write up the issue was something along the lines of:

> An attacker could exploit a vulnerability affecting the RTMPT dissector of `tshark` used by your product to exhaust all CPU resources of the system.

Then, only in the detailed issue analysis, you would mention that your attack vector was the file upload functionality.

The above write-up is factually correct and contains all the required details to be able to see what the problem was, what the impact was and where we should address the issue. I immediately know that **our effort must be focused on getting `tshark` fixed (or just simply upgraded) and NOT on chasing the formidable developers** to change things related to how file names are handled, which would not address our issue. In fact, it would not help us in any way. Just keep this example in mind before you start arguing that arbitrary file upload is a vulnerability.

And this story leads straight into another important topic, which you may already suspect. Do your homework!

# Do Your Homework

Something I personally found really upsetting is that **I had to do the analysis to prove the library’s innocence**. I do understand that a software library is not a human being, but why cannot the “Innocent until proven guilty” work for software libraries too? And, more importantly, why should not proving guilt require indisputable evidence? I realize these are stupid and naive questions... we were never really good at maintaining a functional justice system. Fair trials are not our strength either.

Back to my original point, wouldn’t it be the responsibility of the person reporting the “vulnerability” to perform a thorough analysis and provide indisputable proof? It obviously did not happen. As a result, we have a highly questionable YouTube video that, it seems, MITRE accepted as proof without hesitation and any further analysis. (Just wait until the OWASP section of my post, I will discuss more of this there.)

After some digging, I realized that there must be indeed a problem if there’s a group of professionals trying to create a [replacement](https://lwn.net/Articles/679315/) for the current CVE system. There seem to be many issues with MITRE and my personal issue with it is that they seem to accept things without credible proof. And based on their response to my enquiry, **I think MITRE invented something I could best describe as Opinion-Based Vulnerability Management**.

I took the time to do the analysis for the benefit of everyone. As a result, at first, during some more heated discussions, there were lots of opinions being thrown at me, and the smell of burnt ego and hurt feelings occupied the virtual air. But no facts, no proof, no detailed analysis of any kind came from anyone.

Since then things are not looking so hopeless. Fortunately, I have encountered quite a few sensible people, who even though did not completely see eye to eye, at least made sense.

If I had to come up with a recommendation to penetration testers, especially bug bounty hunters it would be this:

You may get a few peanuts now by rushing to submit your “findings” without putting in the work to do a detailed, sensible technical analysis. However, if you are just focusing on quantity, you are going to lose a lot more in the long run as you will lack the knowledge and understanding to provide a quality service. Focus on quantity and you will hustle ‘till your last breath. Focus on quality and you will never have to hustle again.

# Perspective and Motivation

I have noticed that the people that were arguing with me the most aggressively had a background in penetration testing, either as a professional penetration tester, a bug bounty participant or a mix of those. I also noticed most of them were talking from a “black-box” perspective, which will be important later.

I do believe, that especially some of those participating in bug bounty programs do not see the bigger picture. Yes, finding vulnerabilities is fun (for a while) and getting paid for it is great. But, if you only care about submitting as many issues (most often the same kind across different projects) as you can to earn the most amount of money within the shortest time long term you cause more harm than good. If you rush, you will not have time to perform a proper analysis, prove a vulnerability and communicate it in a way others can understand, really appreciate, and work with the information. The more bug bounty hunters follow the get-rich-quick scheme, the more the bug bounty programs lose value. And when it turns out to be more of an annoyance than added value, that’s when it’s over.

Another motivating factor for a security professional (just as it was for me 15 years ago) to get as many CVEs credited in their name as possible to establish or increase their credibility as a penetration tester.

Most often, the motivation of penetration testers and bug bounty hunters is very limited in scope, focusing mainly on personal gain. Fame and/or money. While the receiving end of the vulnerability reports expects actual help (value) in exchange. I do believe if security professionals prioritized helping out by providing a quality service, they would encounter resistance a lot less frequently. I’ve heard so many times from penetration testers that engineering teams ignored them and sometimes were even hostile. I’ve heard penetration testers complaining during re-tests that most of the previously reported issues were not fixed and they feel that their work was not being valued. After having spent the past few years on the receiving end of penetration testing reports and bug bounty findings, I have to say, I’m no longer surprised.

# OWASP

OWASP, the bible of security professionals. The all-knowing, source of universal truth. Only a few realized and admitted it was seriously outdated and even back in the days it was confusing, and it still is. What could be the reason?

I reached out to someone, a hardcore professional who also contributed to the OWASP project to get his thoughts. One thing I would like to quote here points out the problem:

> imho the line between attacks and vulnerabilities in owasp is narrow

Indeed, that is one big problem. Another is that there are inconsistencies in OWASP materials. But, before showing one of these, let’s have a look at the how OWASP describe Unrestricted File Upload because that is what CVE-2022–29622 was about.

# OWASP: Unrestricted File Upload

![Screenshot 4](./assets/screenshot-4.webp)

First of all, OWASP says here that uploaded files represent a significant **risk**. They are not saying file uploads are a vulnerability. (not yet!) So, it’s a risk that plays a part in an attack: it “helps the attacker accomplish the first step”. Well, so given the context, **it’s an attack vector**.

Then, they say “It depends on what the application does with the uploaded file”. Wait! I’ve heard that somewhere… Then, they say “may trick the application”.

So, basically a CVE has been assigned with **9.8 severity** to something you could best describe as:

> An attack vector presented a potential opportunity for an attacker to attack a potentially vulnerable application. If the application was vulnerable, the malicious actor may have succeeded with his attack.

It sounds pretty ridiculous, right?

Also, is CVSS for calculating a score for attack vectors?

![Screenshot 5](./assets/screenshot-5.webp)

To me, it very much seems CVSS is used for “communicating the characteristics and severity of software **vulnerabilities**”.

Are we seriously blaming an attack vector here? I struggle to understand how SCP or FTP is different from Formidable when it comes to file transfer. Don’t they allow uploading arbitrary files too? Aren’t they attack vectors? None of you ever uploaded a kernel exploit via SCP or FTP? I did! I do not remember any of these randomizing file names, filtering based on extension (by default) or anything like that. Anyway, let’s ignore this logic flaw now because any time I brought this up, there was radio silence… and then just: **Witch! Witch! Witch!**

Then, OWASP provides a massive list of examples, none of which I would question. Those are all legit.

And then...

![Screenshot 6](./assets/screenshot-6.webp)

Suddenly, just like that, after having properly introduced Unrestricted File Upload as an attack vector, correctly not including the word “Vulnerability” in the title of the page, our attack vector turns into a vulnerability. **Jeff Goldblum did not go through such a miraculous transformation in [The Fly](https://www.imdb.com/title/tt0091064/?ref_=fn_al_tt_1) like Unrestricted File Upload**.

OWASP also says that:

> The impact of this “vulnerability is high.

We know from our CVSS calculations that there are a lot more metrics that play a role in the actual severity. Impact (Confidentiality, Integrity and Availability) is just 3 impact categories.

And, more importantly:

> Supposed code can be executed

Well, supposed... I most often used the word “if” instead of “supposed”, maybe that is why people did not comprehend what I was saying. I should have quoted OWASP verbatim on this one.

Just because I set Confidentiality, Integrity and Availability all high in CVSSv3, there are a lot of things that can be tweaked that would produce a final score lot lower than 9.8: [AV:N/AC:H/PR:H/UI:R/S:C/C:H/I:H/A:H/E:U/RL:X/RC:U](https://nvd.nist.gov/vuln-metrics/cvss/v3-calculator?vector=AV:N/AC:H/PR:H/UI:R/S:C/C:H/I:H/A:H/E:U/RL:X/RC:U&version=3.1).

# Final Words

I don’t have many. All adds up, just not the way many of you would expect it to. Best probably to just use a single image that expresses more than I could with words before I “over and out”:

![Screenshot 6](./assets/boom.gif)
