# Application Secrets Handling in a Kubernetes Environment

Author: [Zsolt Imre](http://zsoltimre.com)

Date: 02/04/2022


I had the “pleasure” of working with Kubernetes for the past week and had to think about how to go about sharing credentials with containerised services. It probably won’t come as a surprise that I had a look at the CIS Kubernetes Benchmark (v1.6.0). What may be more interesting is that I found something that came as a surprise, namely control 5.4.1, which says:

> Prefer using secrets as files over secrets as environment variables

It made me curious. I will be honest, my initial though was: what changed so that now we are promoting something that in general is a less secure approach? The answer was to be found under the Rationale section:

> It is reasonably common for application code to log out its environment (particularly in the event of an error).

I think this is a facepalm situation. Before sharing all service configurations via environment variables became a habit, we used configuration files. At that time it was considered safer to share secrets via environment variables. Something has changed... but what?

## Root Cause: Mixing Secrets with Service Configuration

The problem with environment variables is that the secrets and the generic service configuration are mixed. It does not matter whether you store secrets on the file system or environment variables, they are not safe as long as the service configuration is there as well. This sounds very much like I agreed with what the CIS benchmark said... but, I do not!

Generic service configuration (that does not contain secrets) are not confidential. Or, even if you treat them as confidential, it’s not such sensitive information. Secrets are, well, they are best kept secrets. Therefore, which ones you protect more? The secrets! Where are the secrets safer, in environment variables or on the file system (e.g. as mounted secret files)? I believe environment variables are safer. I could explain why, but, that would be a really long post no one would read so instead, let’s focus on and discuss the best way to handle your secrets.

## Secrets in an External Secret Storage

The best way to keep your secrets secret is to use an external secret storage, such as [AWS’s Secrets Manager](https://aws.amazon.com/secrets-manager/). Likely the other cloud service providers have something similar, you can use those too. If those do not work you could try [Harhicorp’s Vault](https://www.hashicorp.com/products/vault) or whatever else that is usable and secure is out there. I will stick to AWS SM in my examples as that’s something I tried and tested.

It’s trivial to create secrets in AWS Secrets Manager. Below you can see one I created for an SMTP connection in a test environment.

![Secret in AWS SM](./assets/aws-sm-secret.png)

What is important is the Secret ARN which you can use to refer to this specific secret. It’s pretty cool you can pass this to your application as an environment variable or within a service configuration file. Then, your application can fetch its own secret from the secrets manager using this this ARN.

Sharing the secret’s ARN with the application as environment variable:

```bash
export SMTP_PASSWD="arn:aws:secretsmanager:eu-west-1:603493154151:secret:smtp-password-qZI610"
```

Beautiful, it’s now safe to log environment variables for debugging purposes.

Sharing the secret’s ARN with the application as part of a service configuration file:

```yaml
...
smtp:
  username: test
  password: arn:aws:secretsmanager:eu-west-1:603493154151:secret:smtp-password-qZI610
...
```

Amazing, even if you logged the value of `password` in your configuration file, or your configuration file got leaked by accident, there are no actual secrets one could use!

Now, there are some important things to consider to really keep your secret a secret. The most important one:

**Applications must be responsible for fetching their own secrets!**

I’ve heard horror stories about developers outsourcing the fetching of service secrets to DevOps. DevOps of course got the secrets fetched (e.g. [https://github.com/external-secrets/external-secrets](https://github.com/external-secrets/external-secrets)) and then Kubernetes passed those secrets to the applications as either environment variables or mounted secrets files. And then, you are back to square one.

What a DevOps engineer should do for you (the developer) is to share secret ARNs with your application. Then, your application should go and fetch the actual secret(s) using the ARN(s). This is a beautiful solution because your secrets won’t show up anywhere, but in the memory of your application. This is a lot safer than having them on the file system or in environment variables. It’s also more elegant, easier to manage, quite easy to implement and if compliance is also of concern: this is an absolute win.

Are our secrets absolutely secure yet? Of course not. Keep reading.

Your application fetches its secrets from the secrets manager so the secrets only appear in memory. This is great until you decide to log/print the real secret. Do not ever log, print or expose your secrets in any other way. You cannot get around this. Again, do not expose your secrets, keep them in memory only! If you make a mistake here, it does not matter where you are getting your secrets from. Act responsibly.

You should also make sure your application does not have any vulnerabilities that would allow an attacker to inject arbitrary code and get it executed. An attacker being able to get arbitrary code executed may be able to dump the application’s memory and extract the secret from the memory dump.

And finally, the security of the environment in which your service is running is also of paramount importance. If an attacker can exploit one or more vulnerabilities in your application to execute arbitrary commands inside the container the attacker could use a tool to dump the applications memory and extract the secret. There should not be any tools in the container that allows to inspect/dump memory and the file system should also be read-only so that the attacker cannot upload such tools.


## Conclusion

 1. **Use an external secrets manager.**
 2. **Applications must fetch their own secrets.** (It’s application domain and not the responsibility of the infrastructure!)
 3. **Do not ever expose your secrets!**
 4. Make sure your application is secure
 5. Make sure the environment in which your application is running is hardened

It’s best to ignore CIS Benchmark 5.4.1 (Prefer using secrets as files over secrets as environment variables), it is flawed.
