# Software Composition Analysis for Startups

Author: [Zsolt Imre](http://zsoltimre.com)

Date: 27/10/2022

It may only be coming from my very own echo chamber produced by the fine algorithms of social media, but it very much seems the whole industry is now talking about BOMs. In this post, I will share our journey with Software Composition Analysis (SCA), BOMs and how we put together a very simple but effective dependency auditing platform using open-source tools. I do this in the hope that others will find it helpful and can get the ball rolling as quickly as possible with minimal effort.

# What is Software Composition Analysis?

SCA is an automated process that helps build more secure software by identifying vulnerable software components and license policy violations.

The high-level process is the following:

 1. **SBOM Generation**: Compile a Software Bill of Materials document (BOM or SBOM for short), an inventory of all software components (dependencies) and specific information about each.
 2. **Submission**: The BOM document is then passed to an SCA tool for analysis.
 3. **Analysis**: The SCA tool, based on the package name and version, looks up publicly available information from various sources about known vulnerabilities affecting the components. In addition, most SCA solutions can check if any dependencies are published under a license that could cause issues for you.
 4. **Reporting**: Finally, the results of the analysis are presented.

SCA should definitely be one of the tools in your pipeline; however, it’s not the alpha and omega of secure software. (It’s a lot closer to the alpha.) You are still responsible for producing secure code yourself. Therefore, you may want to invest some time into threat modelling and embedding static source code analysis and dynamic testing into your pipeline. Anyway, back to SCA. Let’s look at the tools we will use in this post.

# SCA Tooling

Below are the tools we will discuss and set up, organised by the four-step high-level process outlined in the previous section.

 1. **SBOM Generation**: We can say that the de-facto standard is [CycloneDX](https://cyclonedx.org/). If you check out the [CycloneDX Projects on GitHub](https://github.com/orgs/CycloneDX/repositories), you will see that they cover almost all of the most popular programming languages, and it’s quite likely you will find the version of CycloneDX you need to generate SBOM files for your project.
 2. **Submission**: Feeding the SCA tool the generated SBOM file is often quite simple. Some, like Dependency Track which we will discuss later, have a UI that allows uploading the file manually or programmatically using the API. Most often, we will want to do this in an automated fashion and automating it can be as simple as using `curl` to submit the file via the API or by using tools such as [Dtrack.rs](https://gitlab.com/Keymandll/dtrack.rs) that can help enforce your policies and communicate issues more efficiently and proactively to your team. Of course, there’s [dtrackauditor](https://github.com/thinksabin/DTrackAuditor) too, and probably several others. Of course, [Dtrack.rs](https://gitlab.com/Keymandll/dtrack.rs) is your best bet, simply because I wrote it.
 3. **Analysis**: While there are a lot of tools in this category, many of them commercial, I promised open-source; thus, we will jump straight to [Dependency Track](https://dependencytrack.org/). It’s a great tool that does everything we need, and it’s completely free. What else would you need?
 4. **Reporting**: Piece of cake. [Dependency Track](https://dependencytrack.org/) has a nice UI that shows you all the graphs you’ll ever need. You can also look into each component, their vulnerabilities and the vulnerability details. Combine it with Dtrack to upload the SBOMs to Dependency Track, implement a decent audit policy and enforce it in your pipeline and let the developers immediately know about any issues in the pipeline output.

# Putting it All Together

In this section, I will show you how to get all the pieces put together. Let’s start with setting up Dependency Track first.

## Setting up Dependency Track

First of all, you need a server. Wherever that server is up to you and out of scope for this post. One important thing, though, is that you will need at least 4 GB of RAM. Unfortunately, Dependency Track was written in Java and what Java likes the most and eats as much as it can is memory. (Java is the [Cookie Monster](https://en.wikipedia.org/wiki/Cookie_Monster) of RAM) I found that with 4 GB RAM Dependency Track can still run for months without any issues. If I had to estimate, I only had to restart it maximum twice in the last year because things ran out of memory. If you are low on budget, you can improve by shutting down the UI service whenever you do not need it. (Or even better, only starting it when you need it.) I, for example, rarely look at the UI anymore.

Once you have your server up and running and want to get the whole setup done quickly, install and configure [letsencrypt](https://letsencrypt.org/), [Nginx](https://nginx.org/) and [Docker](https://www.docker.com/) next.

Configuring letsencrypt is not covered in this post. Also, it’s quite simple, so I’ll leave it to you. Make sure to start with this, as we will need the certificate and the private key for our Nginx configuration.

Next, add the following `server` entry to your Nginx configuration. Update `server_name`, `ssl_certificate_key` and `ssl_certificate_key` configuration options to match your environment.

```
server {
        listen 443 ssl;
        listen [::]:443 ssl;
        server_name                 sca.example.org;
        ssl_certificate             /path/to/letsencrypt/fullchain.pem;
        ssl_certificate_key         /path/to/letsencrypt/privkey.pem;
        ssl_protocols               TLSv1.2 TLSv1.3;
        ssl_session_cache           shared:SSL:10m;
        ssl_session_timeout         10m;
        ssl_prefer_server_ciphers   on;
        ssl_dhparam                 /etc/nginx/dhparam.pem;

        location /api {
            proxy_pass http://127.0.0.1:49991;
            proxy_set_header Host $http_host;
            rewrite ^/api/?(.*) /$1 break;
            proxy_redirect off;
            client_max_body_size 0;
        }

        location / {
            proxy_pass http://127.0.0.1:49990;
            proxy_set_header Host $http_host;
            proxy_redirect off;
        }
}
```

Finally, use the docker-compose file below to start up Dependency Track. Save the file as `docker-compose.yml`. Update the `DOMAIN`, `POSTGRES_USER` and `POSTGRES_PASSWORD` environment variables to match your requirements. Then, issue the command `docker compose up` from the same directory.

```yaml
version: '3.3'
services:
  dtrack-database:
    image: postgres
    restart: always
    environment:
      DOMAIN: "https://sca.example.org"
      POSTGRES_DB: dtrack
      POSTGRES_USER: dtrack
      POSTGRES_PASSWORD: CHANGE_THIS!
    networks:
      dtrack-database:
  dtrack-apiserver:
    image: dependencytrack/apiserver
    environment:
      ALPINE_DATABASE_MODE: external
      ALPINE_DATABASE_URL: jdbc:postgresql://dtrack-database:5432/${POSTGRES_DB}
      ALPINE_DATABASE_DRIVER: org.postgresql.Driver
      ALPINE_DATABASE_DRIVER_PATH: /extlib/postgresql-42.2.18.jar
      ALPINE_DATABASE_USERNAME: ${POSTGRES_USER}
      ALPINE_DATABASE_PASSWORD: ${POSTGRES_PASSWORD}
    networks:
      dtrack-database:
      dtrack-apiserver:
    depends_on:
      - dtrack-database
    ports:
      - '127.0.0.1:49991:8080'
    volumes:
      - 'dependency-track:/data'
    restart: unless-stopped
  dtrack-frontend:
    image: dependencytrack/frontend
    depends_on:
      - dtrack-apiserver
    environment:
      - API_BASE_URL=https://${DOMAIN}/api
    ports:
      - "127.0.0.1:49990:8080"
    restart: unless-stopped
networks:
  dtrack-database:
  dtrack-apiserver:
volumes:
  dependency-track:
```

If you give it a minute or so, Dependency Track will become accessible on the domain configured. With the above example configuration, it would be `https://sca.example.org`. See the [Initial Startup](https://docs.dependencytrack.org/getting-started/initial-startup/) section of the Dependency Track documentation for the default credentials. Make sure you change the password to a strong passphrase after the first login.

## Setting up SBOM Generation

The best way I can show how simple this is is by showing how I did it in our environment. As we use GitLab, I will show three examples using the GitLab CI configuration (`gitlab-ci.yml`), Rust, Python and JavaScript (Node.JS). I’m sure the examples can be easily adapted to work as part of GitHub Actions.

### Rust

Let’s start with a Rust project because it’s the simplest. In the YAML snippet below, you can see we have three stages in the pipeline: `test`, `audit` and `release`. In our case, the `audit` stage is responsible for generating the SBOM. We install CycloneDX for cargo by running the command `cargo install cargo-cyclonedx`. Then, we run `cargo cyclonedx -f json` to generate the SBOM file, in this case, in JSON format.

```yaml
stages:
  - test
  - audit
  - release
test:
  stage: test
  image: rust:1.64
  script:
    - cargo test
audit:
  stage: audit
  image: rust:1.64
  before_script:
    - cargo install cargo-cyclonedx
  script:
    - cargo cyclonedx -f json
    ...
...
```

### Python

The same looks a bit more verbose for Python, as you can see below.

```yaml
...
audit:
  stage: audit
  script:
    - python3 -m venv sca-venv
    - source sca-venv/bin/activate
    - pip3 install --no-cache .
    - pip3 freeze > requirements.txt
    - pip3 install cyclonedx-bom
    - cyclonedx-py -r -i requirements.txt --format json -o bom.json
...
```

We create a Python virtual environment specifically for SCA-related things and activate it. Then, we install the Python project using `pip`, so we get the list of dependencies using `pip freeze`. It is essential to do this in a clean `venv` so that the generated `requirements.txt` does not get polluted by unrelated dependencies. After, we can install CycloneDX using `pip`. Finally, we generate the BOM file in JSON format from the `requirements.txt` file.

### JavaScript / Node.JS

Again, generating an SBOM file is pretty simple.

```yaml
...
audit:
    script:
        - npm install -g @cyclonedx/bom
        - npm install --only=production
        - cyclonedx-bom --format json -o bom.json
...
```

## SBOM Submission

In this example, I will use [Dtrack.rs](https://gitlab.com/Keymandll/dtrack.rs) to submit the SBOM to Dependency Track. Simply because it’s awesome and also because I’m biased. Check out the README.md file of the Project, as it discusses how to:

 * Set up a Dependency Track API key
 * Configure GitLab to handle the Dependency Track URL and the API key securely without exposing it in the docker-compose file or the job output
 * Optional: Set up a Dtrack policy

Let’s update the Rust BOM generation example from above:

```yaml
stages:
  - test
  - audit
  - release
test:
  stage: test
  image: rust:1.64
  script:
    - cargo test
audit:
  stage: audit
  image: rust:1.64
  before_script:
    - cargo install cargo-cyclonedx
  script:
    - cargo cyclonedx -f json
    - curl --output ./dtrack https://gitlab.com/Keymandll/dtrack.rs/-/package_files/71119808/download
    - chmod 700 ./dtrack
    - dtrack -p example -v 1.0.0 -s bom.json 
...
```

And we are done. Next time you push code into the repository, it will be thoroughly scanned.

## Reporting

You can see the analysis results by logging in to Dependency Track. In addition, if any issues are found, Dtrack will let you know in the Job console.

# Summary

The summary is that I hate writing summaries; therefore, I’m also very bad at it. Alright, let’s be serious. Setting up SCA is not such a big deal, thanks to the amazing open-source community. Investing an hour or two into enrolling your projects in software composition analysis is definitely worth the effort. That’s it. Until next time.
